import React, { Component } from "react";
import PropTypes from "prop-types";
import Profile from "../../Components/Profile/Profile";

function ProfileView(props) {
  return (
    <div className="container profile-container">
      <Profile />
    </div>
  );
}

export default ProfileView;
