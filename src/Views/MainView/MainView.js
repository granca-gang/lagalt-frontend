import React, { Component, useState, useEffect } from "react";
import ProjectList from "../../Components/ProjectList/ProjectList";
import PropTypes from "prop-types";
import styles from "./MainView.module.css";
import globalStyles from "../../Assets/global-styles/bootstrap.module.css";
import cx from "classnames";
import Axios from "axios";
import { useAuth0 } from "../../react-auth0-spa";
import { getIdFromSub } from "../../Utils/utils";
import { useHistory, Link } from "react-router-dom";
import $ from "jquery";

function MainView(props) {
  const history = useHistory();
  const { user, isAuthenticated } = useAuth0();
  const [currentTab, setCurrentTab] = useState("alle");

  //For storing dBUser
  const [dbUser, setDbUser] = useState({});

  const componentIsMounted = React.useRef(true);
  useEffect(() => {
    return () => {
      componentIsMounted.current = false;
    };
  }, []); // Using an empty dependency array ensures this only runs on unmount

  useEffect(() => {
    // if no tab is selected, use vegg
    setCurrentTab("alle");
  }, [props.search]);

  useEffect(() => {
    async function fetchDbUser() {
      let response;
      if (user !== undefined && componentIsMounted)
        try {
          response = await Axios.get(
            `https://lagaltbackend.herokuapp.com/users/${getIdFromSub(
              user.sub
            )}`
          );
          if (componentIsMounted.current) {
            setDbUser(response.data);
          }
        } catch (error) {}
    }

    fetchDbUser();
  }, [user]);

  //Helper function, to set the correct css class
  function checkPath(tab) {
    if (currentTab === tab) {
      return true;
    } else {
      return false;
    }
  }

  if (props.search !== "") {
    console.log(props.search);
    return (
      <div className={cx(globalStyles.container, styles.container)}>
        <h1> Prosjekter </h1>
        <h5>Søkeresultater: </h5>
        <ul
          className={cx("nav", "nav-tabs", styles.nav)}
          id="myTab"
          role="tablist"
          style={{ marginBottom: "20px", marginTop: "16px" }}
        ></ul>
        <ProjectList
          tab={"search"}
          search={"search?name=" + props.search}
          dbUser={dbUser}
        ></ProjectList>
      </div>
    );
  } else {
    return (
      <div className={cx(globalStyles.container, styles.container)}>
        <h1> Prosjekter </h1>
        <ul
          className={cx("nav", "nav-tabs", styles.nav)}
          id="myTab"
          role="tablist"
          style={{ marginBottom: "20px" }}
        >
          <li className="nav-item">
            <a
              className={cx(
                globalStyles["nav-link"],
                styles.projectNav,
                checkPath("alle") ? styles.selected : ""
              )}
              id="alle-fane"
              data-toggle="tab"
              onClick={() => {
                setCurrentTab("alle");
              }}
              href="#alle"
              role="tab"
              aria-controls="alle"
              aria-selected="false"
            >
              Alle
            </a>
          </li>
          {isAuthenticated && (
            <li className="nav-item">
              <a
                className={cx(
                  globalStyles["nav-link"],
                  styles.projectNav,
                  checkPath("vegg") ? cx(styles.selected) : ""
                )}
                id="vegg-fane"
                data-toggle="tab"
                onClick={() => {
                  setCurrentTab("vegg");
                }}
                href="#vegg"
                role="tab"
                aria-controls="vegg"
                aria-selected="true"
              >
                Anbefalte
              </a>
            </li>
          )}
          <li className="nav-item">
            <a
              className={cx(
                globalStyles["nav-link"],
                styles.projectNav,
                checkPath("populaere") ? cx(styles.selected) : ""
              )}
              id="populaere-fane"
              data-toggle="tab"
              onClick={() => {
                setCurrentTab("populaere");
              }}
              href="#populaere"
              role="tab"
              aria-controls="populaere"
              aria-selected={checkPath("#populaere-fane")}
            >
              Populære
            </a>
          </li>
          <li className={cx(globalStyles["nav-item"])}>
            <a
              className={cx(
                globalStyles["nav-link"],
                styles.projectNav,
                checkPath("nye") ? styles.selected : ""
              )}
              id="nye-fane"
              data-toggle="tab"
              onClick={() => {
                setCurrentTab("nye");
              }}
              href="#nye"
              role="tab"
              aria-controls="nye"
              aria-selected="false"
            >
              Nye
            </a>
          </li>
          <li className="nav-item">
            <a
              className={cx(
                globalStyles["nav-link"],
                styles.projectNav,
                checkPath("film") ? cx(styles.selected) : ""
              )}
              id="film-fane"
              data-toggle="tab"
              onClick={() => {
                setCurrentTab("film");
              }}
              href="#film"
              role="tab"
              aria-controls="film"
              aria-selected="false"
            >
              Filmproduksjon
            </a>
          </li>
          <li className="nav-item">
            <a
              className={cx(
                globalStyles["nav-link"],
                styles.projectNav,
                checkPath("spill") ? cx(styles.selected) : ""
              )}
              id="spill-fane"
              data-toggle="tab"
              onClick={() => {
                setCurrentTab("spill");
              }}
              href="#spill"
              role="tab"
              aria-controls="spill"
              aria-selected="false"
            >
              Spillutvikling
            </a>
          </li>
          <li className="nav-item">
            <a
              className={cx(
                globalStyles["nav-link"],
                styles.projectNav,
                checkPath("musikk") ? cx(styles.selected) : ""
              )}
              id="musikk-fane"
              data-toggle="tab"
              onClick={() => {
                setCurrentTab("musikk");
              }}
              href="#musikk"
              role="tab"
              aria-controls="musikk"
              aria-selected="false"
            >
              Musikkproduksjon
            </a>
          </li>
          <li className="nav-item">
            <a
              className={cx(
                globalStyles["nav-link"],
                styles.projectNav,
                checkPath("web") ? cx(styles.selected) : ""
              )}
              id="web-fane"
              data-toggle="tab"
              onClick={() => {
                setCurrentTab("web");
              }}
              href="#web"
              role="tab"
              aria-controls="web"
              aria-selected="false"
            >
              Webutvikling
            </a>
          </li>
        </ul>

        <div className="tab-content" id="myTabContent">
          <div
            className={cx("tab-pane fade show active")}
            id="alle"
            role="tabpanel"
            aria-labelledby="alle-fane"
          >
            <ProjectList
              dbUser={dbUser}
              tab={"#alle-tab"}
              urlExtension="pageable"
              pagination={true}
            ></ProjectList>
          </div>
          {isAuthenticated && (
            <div
              className={cx("tab-pane fade")}
              id="vegg"
              role="tabpanel"
              aria-labelledby="vegg-fane"
            >
              <ProjectList
                tab={"#vegg-tab"}
                recommended={true}
                dbUser={dbUser}
              ></ProjectList>
            </div>
          )}

          <div
            className={cx("tab-pane fade")}
            id="populaere"
            role="tabpanel"
            aria-labelledby="populaere-fane"
          >
            <ProjectList
              dbUser={dbUser}
              tab={"populær prosjekter"}
              urlExtension="popular"
            ></ProjectList>
          </div>
          <div
            className={cx("tab-pane fade")}
            id="nye"
            role="tabpanel"
            aria-labelledby="nye-fane"
          >
            <ProjectList
              dbUser={dbUser}
              tab={"nye prosjekter"}
              urlExtension="new"
              pagination={true}
            ></ProjectList>
          </div>
          <div
            className={cx("tab-pane fade")}
            id="film"
            role="tabpanel"
            aria-labelledby="film-fane"
          >
            <ProjectList
              tab={"film prosjekter"}
              urlExtension={"category/1"}
              dbUser={dbUser}
            ></ProjectList>
          </div>
          <div
            className={cx("tab-pane fade")}
            id="spill"
            role="tabpanel"
            aria-labelledby="spill-fane"
          >
            <ProjectList
              tab={"Spill prosjekter"}
              urlExtension={"category/2"}
              dbUser={dbUser}
            ></ProjectList>
          </div>
          <div
            className={cx("tab-pane fade")}
            id="musikk"
            role="tabpanel"
            aria-labelledby="musikk-fane"
          >
            <ProjectList
              tab={"Musikk prosjekter"}
              urlExtension={"category/3"}
              dbUser={dbUser}
            ></ProjectList>
          </div>
          <div
            className={cx("tab-pane fade")}
            id="web"
            role="tabpanel"
            aria-labelledby="web-fane"
          >
            <ProjectList
              urlExtension={"category/4"}
              dbUser={dbUser}
            ></ProjectList>
          </div>
        </div>
      </div>
    );
  }
}

export default MainView;
