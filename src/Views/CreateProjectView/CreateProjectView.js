import React, { Component, useState, useEffect } from "react";
import PropTypes from "prop-types";
import ProjectForm from "../../Components/ProjectForm/ProjectForm";
import { getIdFromSub } from "../../Utils/utils";
import Axios from "axios";
import { useAuth0 } from "../../react-auth0-spa";
import cx from "classnames";
import styles from "./CreateProjectView.module.css";
import { useHistory } from "react-router-dom";

function CreateProjectView(props) {
  const { isAuthenticated, user, getTokenSilently } = useAuth0();

  let history = useHistory();

  const [newProject, setNewProject] = useState({});
  const [categories, setCategories] = useState({});

  useEffect(() => {
    if (user && newProject) {
      let owner = getIdFromSub(user.sub);
      setNewProject({
        ...newProject,
        ownerId: owner
      });
    }
  }, [user]);

  const handleSubmit = async event => {
    event.preventDefault();
    try {
      const token = await getTokenSilently();
      const response = await Axios.post(
        "https://lagaltbackend.herokuapp.com/projects",
        newProject,
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      ).then(res => history.push(`../../prosjekt/${res.data.id}`));
    } catch (error) {
      console.error(error);
    }
  };

  const handleInputChange = event => {
    setNewProject({
      ...newProject,
      [event.currentTarget.name]: event.currentTarget.value
    });
  };

  const handleCategoryChange = description => {
    let category;
    var x;
    for (x in categories) {
      if (categories[x].description === description) {
        category = {
          id: categories[x].id,
          description: categories[x].description
        };
      }
    }
    setNewProject({
      ...newProject,
      category: category
    });
  };

  //Fetch all categories in database to populate datalist for input selection
  useEffect(() => {
    async function fetchData() {
      const result = await Axios(
        "https://lagaltbackend.herokuapp.com/categories"
      );
      setCategories(result.data);
    }
    fetchData();
  }, []);

  return (
    <div className={cx("container", styles.customForm, styles.projectItem)}>
      <h1>Opprett Prosjekt</h1>
      <div className="row justify-content-center">
        <div className="col-md-8">
          <ProjectForm
            handleSubmit={handleSubmit}
            handleInputChange={handleInputChange}
            categories={categories}
            handleCategoryChange={handleCategoryChange}
          />
        </div>
      </div>
    </div>
  );
}

CreateProjectView.propTypes = {};

export default CreateProjectView;
