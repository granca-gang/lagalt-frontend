import React, { Component } from "react";
import PropTypes from "prop-types";
import Project from "../../Components/Project/Project";
import LabelList from "../../Components/LabelList/LabelList.js";

function ProjectView(props) {
  const projectId = props.match.params.id;
  return (
    <div>
      <Project id={projectId}></Project>
    </div>
  );
}

export default ProjectView;
