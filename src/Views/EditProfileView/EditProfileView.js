import React, { useState, useEffect } from "react";
import cx from "classnames";
import globalStyles from "../../Assets/global-styles/bootstrap.module.css";
import styles from "./EditProfileView.module.css";
import CustomButton from "../../Components/CustomButton/CustomButton";
import LabelList from "../../Components/LabelList/LabelList";
import axios from "axios";
import { useAuth0 } from "../../react-auth0-spa";
import { getIdFromSub } from "../../Utils/utils";
import { isEmptyObject } from "jquery";

function EditProfileView(props) {
  const { isAuthenticated, user, getTokenSilently } = useAuth0();
  const [updateUser, setUpdateUser] = useState({});
  const [labelList, setLabelList] = useState({});
  const [label, setLabel] = useState({});

  //Fetch project data
  useEffect(() => {
    async function fetchData() {
      try {
        if (!isEmptyObject(user)) {
          const result = await axios(
            `https://lagaltbackend.herokuapp.com/users/${getIdFromSub(
              user.sub
            )}`
          );
          console.log(result.data);
          setUpdateUser(result.data);
        }
      } catch (error) {
        console.error(error);
      }
    }
    fetchData();
  }, [user]);

  const handleSubmit = async e => {
    const token = await getTokenSilently();
    const response = await axios
      .put(
        `https://lagaltbackend.herokuapp.com/users`,
        updateUser,

        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(alert("Profilen ble oppdatert!"));
  };

  const handleInputChange = event => {
    setUpdateUser({
      ...updateUser,
      [event.currentTarget.name]: event.currentTarget.value
    });
  };

  useEffect(() => {
    async function fetchData() {
      const result = await axios("https://lagaltbackend.herokuapp.com/labels");
      setLabelList(result.data);
    }
    fetchData();
  }, []);

  const addLabel = async () => {
    var x;
    var newLabel;
    for (x in labelList) {
      if (labelList[x].label === label.label) {
        newLabel = { id: labelList[x].id, label: labelList[x].label };
        var updateLabels = updateUser.labels;
        updateLabels.push(newLabel);
        setUpdateUser({
          ...updateUser,
          labels: updateLabels
        });
      }
    }
    try {
      const token = await getTokenSilently();
      const response = await axios.put(
        `https://lagaltbackend.herokuapp.com/users/${updateUser.id}/labels`,
        newLabel,
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      );
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className={(globalStyles.container, styles.container)}>
      <h1>Oppdater profil</h1>
      <div
        className={cx(
          globalStyles.row,
          globalStyles["justify-content-center"],
          styles.projectItem
        )}
      >
        <div className={cx("col-md-5")}>
          <div className={styles.description}>
            <h4>Informasjon</h4>
            <form
              className={cx(globalStyles["form-group"], styles.customForm)}
              onSubmit={handleSubmit}
            >
              <label className={styles.customForm}>
                Navn:
                <input
                  name="name"
                  className={cx(
                    globalStyles["form-control"],
                    styles["customInput"]
                  )}
                  type="text"
                  value={updateUser.name}
                  required
                  onChange={handleInputChange}
                />
              </label>
              <br></br>
              <label className={styles.customForm}>
                Email:
                <input
                  name="email"
                  className={cx(
                    globalStyles["form-control"],
                    styles["customInput"]
                  )}
                  type="text"
                  value={updateUser.email}
                  required
                  onChange={handleInputChange}
                />
              </label>
              <br></br>
              <label className={styles.customForm}>
                Synlighet:
                <select
                  name="hidden"
                  className={cx(
                    globalStyles["form-control"],
                    styles["customInput"]
                  )}
                  type="text"
                  required
                  onChange={handleInputChange}
                >
                  {updateUser.hidden && (
                    <>
                      <option value={true} defaultValue>
                        Skjult
                      </option>
                      <option value={false}>Synlig</option>
                    </>
                  )}
                  {!updateUser.hidden && (
                    <>
                      <option value={false} defaultValue>
                        Synlig
                      </option>
                      <option value={true}>Skjult</option>
                    </>
                  )}
                </select>
              </label>
              <br></br>
              <label>
                Beskrivelse:
                <textarea
                  name="description"
                  rows="5"
                  cols="50"
                  className={cx(
                    globalStyles["form-control"],
                    styles["form-textarea"],
                    styles["customInput"]
                  )}
                  value={updateUser.description || ""}
                  required
                  onChange={handleInputChange}
                />
              </label>
              <br></br>
              <CustomButton
                type="submit"
                value="Bekreft endringer"
              ></CustomButton>
            </form>
          </div>
        </div>
        <div className={cx("col-md-3")}>
          <div className={styles.description}>
            <h4>Ferdigheter</h4>
            <label>
              <input
                className={cx(
                  globalStyles["form-control"],
                  styles["customInput"]
                )}
                type="text"
                name="label"
                list="labels"
                id="project_labels"
                autoComplete="off"
                required
                value={label.label}
                onChange={event =>
                  setLabel({ id: event.target.key, label: event.target.value })
                }
              />
              <datalist id="labels">
                {Object.keys(labelList).map((key, i) => {
                  return (
                    <option key={labelList[key].id}>
                      {labelList[key].label}
                    </option>
                  );
                })}
              </datalist>
              <CustomButton
                onClick={addLabel}
                type="button"
                value="Legg til"
              ></CustomButton>
              <LabelList labels={updateUser.labels} />
            </label>
          </div>
        </div>
      </div>
    </div>
  );
}

export default EditProfileView;
