import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import OtherProfile from "../../Components/OtherProfile/OtherProfile";
import axios from "axios";
import globalStyles from "../../Assets/global-styles/bootstrap.module.css";
import cx from "classnames";

function OtherProfileView(props) {
  const profileId = props.match.params.id;
  const [isHidden, setIsHidden] = useState(true);

  useEffect(() => {
    async function fetchIsHidden() {
      const result = await axios(
        `https://lagaltbackend.herokuapp.com/users/${profileId}/ishidden`
      );
      setIsHidden(result.data);
      console.log(isHidden);
    }
    fetchIsHidden();
  }, [isHidden, profileId, setIsHidden]);

  if (isHidden === true) {
    return (
      <div
        className="container hidden-user"
        style={{ color: "white", height: "500px" }}
      >
        <div className={cx(globalStyles.row, globalStyles["h-50"])}></div>
        <div className={cx(globalStyles.row)}>
          <div className={globalStyles["col-md"]}>
            <h3>
              Denne brukeren eksisterer ikke eller har skjult profilen sin...
            </h3>
          </div>
        </div>
        <div className={(cx(globalStyles.row), globalStyles["h-50"])}></div>
      </div>
    );
  } else {
    return (
      <div className="container profile-container">
        <OtherProfile profileId={profileId} />
      </div>
    );
  }
}

export default OtherProfileView;
