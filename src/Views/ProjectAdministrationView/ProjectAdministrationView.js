import React, { useState, useEffect } from "react";
import cx from "classnames";
import globalStyles from "../../Assets/global-styles/bootstrap.module.css";
import styles from "./ProjectAdministrationView.module.css";
import CustomButton from "../../Components/CustomButton/CustomButton";
import LabelList from "../../Components/LabelList/LabelList";
import axios from "axios";
import { useAuth0 } from "../../react-auth0-spa";
import ProjectAdministration from "../../Components/ProjectAdministration/ProjectAdministration";
import AdministrateMember from "../../Components/AdministrateMember/AdministrateMember";
import AdministrateApplicants from "../../Components/AdministrateApplicant/AdministrateApplicant";
import Axios from "axios";

function ProjectAdministrationView(props) {
  const projectId = props.match.params.id;
  const { isAuthenticated, user, getTokenSilently } = useAuth0();

  const [project, setProject] = useState({
    category: {},
    labels: []
  });

  const [applicants, setApplicants] = useState({
    user: {},
    project: {},
    role: ""
  });

  const [application, setApplication] = useState(false);
  //Fetch project data
  useEffect(() => {
    async function fetchData() {
      try {
        const result = await axios(
          `https://lagaltbackend.herokuapp.com/projects/${projectId}`
        );
        setProject(result.data);
      } catch (error) {
        console.error(error);
        setProject("deleted");
      }
    }
    fetchData();
  }, []);

  const [members, setMembers] = useState({ members: [] });

  useEffect(() => {
    async function fetchData() {
      const result = await axios(
        `https://lagaltbackend.herokuapp.com/projects/${projectId}/members`
      );
      setMembers(result.data);
    }
    fetchData();
  }, [applicants]);

  useEffect(() => {
    async function fetchData() {
      const result = await axios(
        `https://lagaltbackend.herokuapp.com/projects/${projectId}/applicants`
      );
      setApplicants(result.data);
    }
    fetchData();
  }, []);

  const removeUser = async user => {
    try {
      console.log(user);
      const token = await getTokenSilently();
      const response = await Axios.delete(
        `https://lagaltbackend.herokuapp.com/projects/${projectId}/users/${user.id}`,
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      );
      setApplicants({});
      setMembers({});
    } catch (error) {
      console.error(error);
    }
  };

  const [applicant, setApplicant] = useState({
    user: {},
    project: {},
    role: ""
  });

  const addUser = user => {
    const accept = {
      id: user.userProjectId,
      user: user,
      project: project,
      role: "MEMBER"
    };

    putApplication(accept);
    // setApplication(true)
  };

  const putApplication = async applicant => {
    console.log(applicant);
    try {
      const token = await getTokenSilently();
      const response = await Axios.put(
        `https://lagaltbackend.herokuapp.com/userprojects`,
        applicant,
        {
          headers: {
            Authorization: `Bearer ${token} `
          }
        }
      );
      setMembers({});
      setApplicants({});
    } catch (error) {
      console.error(error);
    }
  };

  const deleteProject = async () => {
    try {
      const token = await getTokenSilently();
      const response = await Axios.delete(
        `https://lagaltbackend.herokuapp.com/projects/${projectId}`,
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      );
    } catch (error) {
      console.error(error);
    }
    setProject("deleted");
  };

  if (project === "deleted" || project === {}) {
    return (
      <div className={cx("container", styles.customForm)}>
        Prosjektet har blitt slettet.
      </div>
    );
  }

  return (
    <div className={cx("container", styles.customForm)}>
      <h1>Prosjekt Administrasjon</h1>
      <h4>Informasjon</h4>
      <ProjectAdministration project={project} />
      <div
        className={cx(
          styles.container,
          globalStyles.row,
          globalStyles["justify-content-center"],
          styles.projectItem,
          styles.margins
        )}
      >
        <div className={cx("col-md-11")}>
          <div className={styles.description}>
            <h4>Medlemmer</h4>
            {Object.keys(members).map((key, i) => {
              return (
                <AdministrateMember
                  user={members[key]}
                  key={members[key].id}
                  removeMember={event => removeUser(members[key])}
                />
              );
            })}
          </div>
        </div>
      </div>

      <div
        className={cx(
          styles.container,
          globalStyles.row,
          globalStyles["justify-content-center"],
          styles.projectItem,
          styles.margins
        )}
      >
        <div className={cx("col-md-11")}>
          {applicants.length > 0 && (
            <div className={styles.description}>
              <div>
                <h4>Ønsker tilgang</h4>
                {Object.keys(applicants).map((key, i) => {
                  return (
                    <AdministrateApplicants
                      user={applicants[key]}
                      project={project}
                      key={applicants[key].id}
                      addUser={event => addUser(applicants[key])}
                      removeUser={event => removeUser(applicants[key])}
                    />
                  );
                })}
                <hr />
              </div>
            </div>
          )}
        </div>
      </div>

      <button class="btn btn-danger" data-toggle="modal" data-target="#accept">
        Avslutt prosjekt
      </button>
      <div
        class="modal fade"
        id="accept"
        tabindex="-1"
        role="dialog"
        aria-labelledby="acceptLabel"
        aria-hidden="true"
      >
        <div class="modal-dialog" role="document" style={{ color: "black" }}>
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="accpetLabel">
                Bekreft
              </h5>
              <button
                type="button"
                class="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>
                Ved å avslutte prosjektet mister du tilgang til brukerne i
                prosjektet og sletter all informasjon. Er du sikker på du vil
                avslutte?
              </p>
            </div>
            <div class="modal-footer">
              <button
                type="button"
                class="btn btn-secondary"
                data-dismiss="modal"
              >
                Avslå
              </button>
              <form>
                <button
                  type="button"
                  class="btn btn-danger"
                  data-dismiss="modal"
                  onClick={event => deleteProject()}
                >
                  Slett...
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ProjectAdministrationView;
