// src/react-auth0-spa.js
import React, { useState, useEffect, useContext } from "react";
import createAuth0Client from "@auth0/auth0-spa-js";
import Axios from "axios";

const DEFAULT_REDIRECT_CALLBACK = () =>
  window.history.replaceState({}, document.title, window.location.pathname);

export const Auth0Context = React.createContext();
export const useAuth0 = () => useContext(Auth0Context);
export const Auth0Provider = ({
  children,
  onRedirectCallback = DEFAULT_REDIRECT_CALLBACK,
  ...initOptions
}) => {
  const [isAuthenticated, setIsAuthenticated] = useState();
  const [user, setUser] = useState();
  const [auth0Client, setAuth0] = useState();
  const [loading, setLoading] = useState(true);
  const [popupOpen, setPopupOpen] = useState(false);

  useEffect(() => {
    const initAuth0 = async () => {
      const auth0FromHook = await createAuth0Client(initOptions);
      setAuth0(auth0FromHook);

      if (
        window.location.search.includes("code=") &&
        window.location.search.includes("state=")
      ) {
        const { appState } = await auth0FromHook.handleRedirectCallback();
        onRedirectCallback(appState);
      }

      const isAuthenticated = await auth0FromHook.isAuthenticated();

      setIsAuthenticated(isAuthenticated);

      if (isAuthenticated) {
        const user = await auth0FromHook.getUser();
        setUser(user);
      }

      setLoading(false);
    };
    initAuth0();
    // eslint-disable-next-line
  }, []);

  const loginWithPopup = async (params = {}) => {
    setPopupOpen(true);
    try {
      await auth0Client.loginWithPopup(params);
    } catch (error) {
      console.error(error);
    } finally {
      setPopupOpen(false);
    }
    const user = await auth0Client.getUser();
    const regex = /(.*)\|/;

    let response;
    //Checks if user exists in the database
    try {
      response = await Axios.get(
        `https://lagaltbackend.herokuapp.com/users/${user.sub.replace(
          regex,
          ""
        )}`
      );
    } catch (error) {
      //If error and server responds with 404 not found, response is set to false.
      //This prevents the user from being logged in at auth0, while our server is down.
      if (error.response && error.response.status === 404) {
        response = false;
      } else {
        auth0Client.logout({
          returnTo: window.location.origin
        });
      }
    }

    const token = await auth0Client.getTokenSilently();
    console.log(response);

    //If user exists update with new valuses, for example if user changed picture.
    if (response) {
      try {
        await Axios.put(
          `https://lagaltbackend.herokuapp.com/users/`,
          {
            labels: response.data.labels, //[{ id: 1, label: "Programmering" }],
            visitedProjects: response.data.visitedProjects,
            id: `${user.sub.replace(regex, "")}`,
            name: user.name,
            email: user.email,
            picture: user.picture,
            role: response.data.role,
            hidden: response.data.hidden,
            description: response.data.description
          },
          {
            headers: {
              Authorization: `Bearer ${token}`
            }
          }
        );
      } catch (error) {
        return;
      }
      //Create new user
    } else {
      try {
        await Axios.post(
          `https://lagaltbackend.herokuapp.com/users/`,
          {
            labels: [],
            visitedProjects: [],
            id: `${user.sub.replace(regex, "")}`,
            name: user.name,
            email: user.email,
            picture: user.picture,
            role: "user",
            hidden: false
          },
          {
            headers: {
              Authorization: `Bearer ${token}`
            }
          }
        );
      } catch (error) {
        return;
      }
    }

    /**
     * If user exists, append db information
     * and set user, else new user from information, then append.
     */

    setUser(user);
    setIsAuthenticated(true);
    //
  };

  const handleRedirectCallback = async () => {
    setLoading(true);
    await auth0Client.handleRedirectCallback();
    const user = await auth0Client.getUser();
    setLoading(false);
    setIsAuthenticated(true);
    setUser(user);
  };
  return (
    <Auth0Context.Provider
      value={{
        isAuthenticated,
        user,
        loading,
        popupOpen,
        loginWithPopup,
        handleRedirectCallback,
        getIdTokenClaims: (...p) => auth0Client.getIdTokenClaims(...p),
        loginWithRedirect: (...p) => auth0Client.loginWithRedirect(...p),
        getTokenSilently: (...p) => auth0Client.getTokenSilently(...p),
        getTokenWithPopup: (...p) => auth0Client.getTokenWithPopup(...p),
        logout: (...p) => auth0Client.logout(...p)
      }}
    >
      {children}
    </Auth0Context.Provider>
  );
};
