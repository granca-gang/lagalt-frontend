import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import globalStyles from "../../Assets/global-styles/bootstrap.module.css";
import cx from "classnames";
import styles from "./OtherProfile.module.css";
import axios from "axios";
import LabelList from "../LabelList/LabelList";
import ListButton from "../ListButton/ListButton";

function Profile(props) {
  const [dbUser, setDbUser] = useState({
    labels: []
  });

  const [userProjects, setUserProjects] = useState({});

  useEffect(() => {
    async function fetchDbUser() {
      try {
        const userResponse = await axios(
          `https://lagaltbackend.herokuapp.com/users/${props.profileId}`
        );
        setDbUser(userResponse.data);
        console.log("hade");
      } catch (error) {
        console.error(error);
      }
    }
    fetchDbUser();
  }, [props.profileId]);

  useEffect(() => {
    async function fetchUserProjects() {
      try {
        const response = await axios(
          `https://lagaltbackend.herokuapp.com/users/${props.profileId}/projects`
        );
        console.log("hej");
        setUserProjects(response.data);
      } catch (error) {
        console.error(error);
      }
    }
    fetchUserProjects();
  }, [props.profileId]);

  return (
    <div className={styles.profile}>
      <h1>Brukerprofil {"  "}</h1>

      <div className={globalStyles.row}>
        <div className={cx(styles["user-profile"], globalStyles["col-md-8"])}>
          <h3>{dbUser.name}</h3>
          <div className={globalStyles.row}>
            <div className={globalStyles["col-md-8"]}>
              <p>E-mail: {dbUser.email}</p>
              <div className={styles["user-info"]}>
                <p>{dbUser.description}</p>
              </div>
            </div>
            <div className={globalStyles["col-sm-4"]}>
              <figure className={globalStyles.figure}>
                <img
                  className={cx(globalStyles["figure-img"], styles.image)}
                  src={dbUser.picture}
                  alt="dbUser"
                ></img>
              </figure>
            </div>
          </div>
          <div className={globalStyles.row}>
            <div className={cx(globalStyles["col-12"], styles["user-skills"])}>
              <LabelList />
            </div>
          </div>
        </div>
        <div className={cx(globalStyles["col-sm"], styles["user-project"])}>
          <h4>Brukerprosjekter</h4>
          <ul className="list-group">
            {Object.keys(userProjects).map((key, i) => {
              return (
                <ListButton
                  value={userProjects[key].name}
                  to={`/prosjekt/${userProjects[key].id}`}
                  key={userProjects[key].id}
                />
              );
            })}
          </ul>
        </div>
      </div>
    </div>
  );
}
export default Profile;
