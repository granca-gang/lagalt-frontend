import React, { Component } from "react";
import PropTypes from "prop-types";
import globalStyles from "../../Assets/global-styles/bootstrap.module.css";
import cx from "classnames";
import CustomButton from "../CustomButton/CustomButton";
import { Link } from "react-router-dom";

function AdministrateMember(props) {
  return (
    <div
      className="d-flex justify-content-between"
      style={{ borderBottom: "1px solid white", borderRadius: "4px" }}
    >
      <Link
        to={`/profil/${props.user.id}`}
        style={{ textDecoration: "none", color: "white" }}
      >
        <h6 style={{ paddingTop: "25px" }}>{props.user.name}</h6>
      </Link>
      <div>
        <CustomButton value="Fjern medlem" onClick={props.removeMember} />
      </div>
    </div>
  );
}

export default AdministrateMember;
