import React, { Component } from "react";
import PropTypes from "prop-types";
import styles from "./LabelItem.module.css";

function LabelItem(props) {
  return <span className={styles.tags}>{props.label}</span>;
}

LabelItem.propTypes = {};

export default LabelItem;
