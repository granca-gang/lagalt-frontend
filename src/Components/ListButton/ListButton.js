import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import styles from "./ListButton.module.css";
import globalStyles from "../../Assets/global-styles/bootstrap.module.css";
import cx from "classnames";

function ListButton(props) {
  return (
    <div style={{ textDecoration: "none" }}>
      <Link to={props.to} style={{ textDecoration: "none" }}>
        <button
          type="button"
          className={cx(
            globalStyles["list-group-item"],
            globalStyles["list-group-item-action"],
            styles["list-button"]
          )}
          onClick={props.onClick}
        >
          {props.value}
        </button>
      </Link>
    </div>
  );
}

ListButton.propTypes = {};

export default ListButton;
