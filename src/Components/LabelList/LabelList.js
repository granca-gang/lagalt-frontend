import React, { Component } from "react";
import PropTypes from "prop-types";
import LabelItem from "../LabelItem/LabelItem";
import cx from "classnames";
import globalStyles from "../../../node_modules/bootstrap/dist/css/bootstrap.css";
import styles from "./LabelList.module.css";

function LabelList(props) {
  const labels = props.labels;

  if (labels === undefined) {
    return <div></div>;
  } else {
    return (
      <div className={styles.labels}>
        {labels.map((label, index) => {
          return <LabelItem key={index} label={label.label} />;
        })}
      </div>
    );
  }
}

LabelList.propTypes = {};

export default LabelList;
