import React, { Component } from "react";
import PropTypes from "prop-types";
import styles from "./CustomButton.module.css";
import globalStyles from "../../Assets/global-styles/bootstrap.module.css";
import cx from "classnames";

function CustomButton(props) {
  return (
    <button
      className={cx(styles.customButton, globalStyles["btn-round"], styles.btn)}
      onClick={props.onClick}
      type={props.type}
    >
      {props.value}
    </button>
  );
}

CustomButton.propTypes = {};

export default CustomButton;
