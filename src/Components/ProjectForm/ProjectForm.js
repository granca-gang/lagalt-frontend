import React, { useState } from "react";
import PropTypes from "prop-types";
import globalStyles from "../../Assets/global-styles/bootstrap.module.css";
import cx from "classnames";
import styles from "./ProjectForm.module.css";
import CustomButton from "../CustomButton/CustomButton";

function ProjectForm(props) {
  const [picture, setPicture] = useState();

  return (
    <div className={cx(styles.createProject, styles.description)}>
      <form
        className={cx(globalStyles["form-group"])}
        onSubmit={props.handleSubmit}
      >
        <div className="row">
          <div className="col-md-5">
            <label className={styles.label}>
              Navn:
              <input
                name="name"
                className={cx(
                  globalStyles["form-control"],
                  styles["customInput"]
                )}
                type="text"
                required
                onChange={props.handleInputChange}
                placeholder="Navn"
              />
            </label>
            <br />
            <label className={styles.label}>
              Kategori:
              <select
                className={styles.customInput}
                onChange={event => {
                  props.handleCategoryChange(event.target.value);
                }}
                id="categories"
              >
                <option selected> Velg Kategori</option>
                {Object.keys(props.categories).map((key, i) => {
                  return (
                    <option key={props.categories[key].id}>
                      {props.categories[key].description}
                    </option>
                  );
                })}
              </select>
            </label>
            <br></br>
            <label className={styles.label}>
              Status:
              <input
                name="status"
                className={cx(
                  globalStyles["form-control"],
                  styles["customInput"]
                )}
                onChange={props.handleInputChange}
                required
                placeholder="Status"
              />
            </label>
          </div>
          <div className="col-md">
            {<img className={styles.img} src={picture} alt={picture}></img>}
          </div>
        </div>
        <label className={styles.label}>
          Bilde:
          <input
            name="picture"
            required
            className={cx(globalStyles["form-control"], styles["customInput"])}
            type="text"
            onChange={event => {
              props.handleInputChange(event);
              setPicture(event.target.value);
            }}
            placeholder="Link til prosjektbilde"
          />
        </label>

        <label className={styles.label}>
          Link:
          <input
            name="link"
            className={cx(globalStyles["form-control"], styles["customInput"])}
            type="text"
            onChange={props.handleInputChange}
            placeholder="Link til relatert prosjekt side. (YouTube, Soundcloud, hjemmeside)"
          />
        </label>

        <label className={styles.label}>
          Git Repo:
          <input
            name="git"
            className={cx(globalStyles["form-control"], styles["customInput"])}
            type="text"
            onChange={props.handleInputChange}
            placeholder="Link til offentlig git repo fra GitHub eller GitLab"
          />
        </label>

        <label>
          Beskrivelse:
          <textarea
            name="description"
            rows="5"
            cols="50"
            className={cx(
              globalStyles["form-control"],
              styles["form-textarea"],
              styles["customInput"]
            )}
            onChange={props.handleInputChange}
            type="text"
            required
            placeholder="Beskrivelse av prosjektet"
          />
        </label>
        <div style={{ position: "absolute", right: "20px", bottom: "20px" }}>
          <CustomButton type="submit" value="Send"></CustomButton>
        </div>
      </form>
    </div>
  );
}

export default ProjectForm;
