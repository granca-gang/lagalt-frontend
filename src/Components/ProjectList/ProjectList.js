import React, { useEffect, useState } from "react";
import ProjectListItem from "../ProjectListItem/ProjectListItem";
import Axios from "axios";
import { useAuth0 } from "../../react-auth0-spa";
import isEmptyObject from "jquery";
import cx from "classnames";
import styles from "./ProjectList.module.css";

function ProjectList(props) {
  const { isAuthenticated, user, getTokenSilently } = useAuth0();
  const [projects, setProjects] = useState([]);
  const [page, setPage] = useState(0);
  const [displayLoadButton, setDisplayLoadButton] = useState(props.pagination);

  const componentIsMounted = React.useRef(true);
  useEffect(() => {
    return () => {
      componentIsMounted.current = false;
    };
  }, []); // Using an empty dependency array ensures this only runs on unmount

  useEffect(() => {
    async function fetchRecommended() {
      //cheks if it is the recommended tab

      if (props.recommended === true) {
        console.log("recommended: ", props.recommended);
        try {
          if (isAuthenticated) {
            const token = await getTokenSilently();
            console.log(token);
            const result = await Axios.get(
              "https://lagaltbackend.herokuapp.com/projects/recommended",
              {
                headers: {
                  Authorization: `Bearer ${token}`
                }
              }
            );

            if (componentIsMounted.current) {
              setProjects(result.data);
            }
          }
        } catch (error) {
          console.error(error);
        }
      }
    }
    fetchRecommended();
  }, [user, isAuthenticated, getTokenSilently, props.recommended]);

  useEffect(() => {
    async function fetchSearch() {
      //cheks if it is the recommended tab
      if (props.search && props.search !== "") {
        try {
          console.log(props.search);

          const result = await Axios.get(
            `https://lagaltbackend.herokuapp.com/projects/${props.search}`
          );

          if (componentIsMounted.current) {
            setProjects(result.data);
          }
        } catch (error) {
          console.error(error);
        }
      }
    }
    fetchSearch();
  }, [user, isAuthenticated, getTokenSilently, props.search]);

  useEffect(() => {
    async function fetchData() {
      let url = "https://lagaltbackend.herokuapp.com/projects/";

      if (props.urlExtension !== "recommended" && props.search === undefined) {
        if (props.urlExtension) {
          url += props.urlExtension;
        }
        try {
          let result;
          console.log(props.urlExtension);

          if (props.pagination) {
            console.log("Got here");
            url += "?page=" + page;
          }

          result = await Axios.get(url);

          if (componentIsMounted.current) {
            if (props.pagination) {
              if (projects !== result.data.content)
                console.log(props.tab, result.data);
              setProjects(
                [...projects, ...result.data.content].filter(
                  (project, index, array) =>
                    array.findIndex(
                      tempProject => tempProject.id === project.id
                    ) === index
                )
              );
              setDisplayLoadButton(!result.data.last);
            } else {
              setProjects(result.data);
            }
          }
        } catch (error) {
          console.error(error);
        }
      }
    }
    fetchData();
  }, [props.urlExtension, page, props.pagination]);

  if (projects.length === 0 && props.search) {
    return <div>Beklager, ingen treff! </div>;
  } else if (projects.length === 0 || projects === undefined) {
    return <div>Henter prosjekter, vennligst vent..</div>;
  } else {
    return (
      <div>
        {projects.map(project => {
          return (
            <ProjectListItem
              key={project.id}
              project={project}
              dbUser={props.dbUser}
            ></ProjectListItem>
          );
        })}
        {displayLoadButton && props.pagination && (
          <button
            className={cx(styles.btn, styles.customButton)}
            onClick={() => setPage(page + 1)}
          >
            {" "}
            Last inn flere{" "}
          </button>
        )}
        {!displayLoadButton && props.pagination && (
          <p> Alle prosjekter er lastet inn</p>
        )}
      </div>
    );
  }
}

export default ProjectList;
