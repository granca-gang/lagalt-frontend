import React, { Component, useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import globalStyles from "../../Assets/global-styles/bootstrap.module.css";
import styles from "./ProjectListItem.module.css";
import cx from "classnames";
import Axios from "axios";
import { useAuth0 } from "../../react-auth0-spa";
import LabelList from "../LabelList/LabelList";
import { getIdFromSub } from "../../Utils/utils";

function ProjectListItem(props) {
  const regex = /(.{0,270})( )/m;

  const { isAuthenticated, user } = useAuth0();

  const [needed, setNeeded] = useState();

  useEffect(() => {
    async function fetchNeeded() {
      if (isAuthenticated && user) {
        try {
          const neededResponse = await Axios(
            `https://lagaltbackend.herokuapp.com/projects/${props.project.id}/label/needed`
          );

          setNeeded(neededResponse.data);
        } catch (error) {
          console.error(error);
          setNeeded([]);
        }
      }
    }

    fetchNeeded();
  }, [props.project.id, isAuthenticated, user]);

  function displayDescription(string) {
    let m;
    if ((m = regex.exec(string)) !== null && string.length > 280) {
      return m[0] + "...";
    } else {
      return string;
    }
  }

  function checkLabel(id) {
    if (props.dbUser.labels) {
      if (props.dbUser.labels.find(label => label.id === id)) {
        return true;
      } else {
        return false;
      }
    }
  }

  function checkNeeded(id) {
    if (needed && needed.find(label => label.id === id)) {
      return <i className="fas fa-exclamation"></i>;
    } else {
      return null;
    }
  }

  function iconSwitch(category) {
    switch (category.id) {
      case 1:
        return (
          <i className={cx("fas fa-film", styles.icon, styles.film)}>
            <span className={styles.tooltiptext}>{category.description}</span>
          </i>
        );
      case 2:
        return (
          <i className={cx("fas fa-gamepad", styles.icon)}>
            <span className={styles.tooltiptext}>{category.description}</span>
          </i>
        );
      case 3:
        return (
          <i className={cx("fas fa-music", styles.icon)}>
            <span className={styles.tooltiptext}>{category.description}</span>
          </i>
        );
      case 4:
        return (
          <i className={cx("fas fa-laptop-code", styles.icon)}>
            <span className={styles.tooltiptext}>{category.description}</span>
          </i>
        );
      default:
        return null;
    }
  }

  return (
    <Link
      to={`/prosjekt/${props.project.id}`}
      key={props.project.id}
      style={{ textDecoration: "none" }}
    >
      <div
        className={cx(globalStyles.row, styles.row, styles.center, "d-flex")}
      >
        <div className={globalStyles["col-md-3"]}>
          <img
            src={
              props.project.picture
                ? props.project.picture
                : "https://www.publicdomainpictures.net/pictures/190000/velka/black-background-1468370534d5s.jpg"
            }
            className="card-img"
            alt="Project"
            style={{ width: "100%", height: "160px" }}
          />
        </div>
        <div className="col">
          <div>
            <div className="d-flex justify-content-between">
              <h5 className="card-title">{props.project.name}</h5>

              {iconSwitch(props.project.category)}
            </div>
            <p className={cx(globalStyles["card-text"], styles.text)}>
              {displayDescription(props.project.description)}
            </p>
          </div>
          <div className={cx(globalStyles["col-md-9"])}>
            <div className="d-flex">
              {props.project.labels.map(label => {
                return (
                  <p
                    className={cx(
                      styles.tags,
                      checkLabel(label.id) ? styles.match : ""
                    )}
                    key={label.id}
                  >
                    {label.label + " "} {checkNeeded(label.id)}
                  </p>
                );
              })}
            </div>
          </div>
        </div>
      </div>

      {/* <button className="no-border">
        <div
          className="card mb-3 justify-content-center text-left"
          style={{
            maxWidth: "960px",
            maxHeight: "200px",
            marginLeft: "Auto",
            marginRight: "Auto",
            padding: "5px"
          }}
        >
          <div className="row no-gutters">
            <div className="col-md-2">
              <img
                src={props.urlprop}
                className="card-img"
                alt="..."
                style={{ width: "160px", height: "160px" }}
              />
            </div>
            <div className="col-md-9">
              <div className="card-body">
                <h5 className="card-title">Card title</h5>
                <p className="card-text block-with-text">
                  This is a wider card with supporting text below as a natural
                  lead-in to additional content. This content is a little bit
                  longer. This is a wider card with supporting text below as a
                  natural lead-in to additional content. This content is a
                  little bit longer.This is a wider card with supporting text
                  below as a natural lead-in to additional content. This content
                  is a little bit longer. This is a wider card with supporting
                  text below as a natural lead-in to additional content. This
                  content is a little bit longer.
                </p>
                <p className="card-text">
                  <small className="text-muted">Last updated 3 mins ago</small>
                </p>
              </div>
            </div>
          </div>
        </div>
        </button> */}
    </Link>
  );
}

export default ProjectListItem;

/*


*/
