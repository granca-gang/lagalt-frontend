import React, { useState, useEffect, useRef } from "react";
import globalStyles from "../../Assets/global-styles/bootstrap.module.css";
import cx from "classnames";
import axios from "axios";
import styles from "./ProjectLink.module.css";
import ListButton from "../ListButton/ListButton";
import { Link, Router } from "react-router-dom";
import { useAuth0 } from "../../react-auth0-spa";
import Axios from "axios";
import { getIdFromSub } from "../../Utils/utils";
import { isEmptyObject } from "jquery";
import ProjectMember from "../ProjectMember/ProjectMember";

function ProjectLink(props, { match }) {
  const { isAuthenticated, getTokenSilently } = useAuth0();
  const [category, setCategory] = useState("");

  function getApiLink(link) {
    if (link !== undefined) {
      const gitlabRegEx = /(?:https:\/\/gitlab.com\/)(.*)/;
      const githubRegEx = /(?:https:\/\/github.com\/)(.*)/;

      if (link.toLowerCase().includes("gitlab")) {
        const matches = link.match(gitlabRegEx);
        console.log(matches[1]);
        setCategory("gitlab");
        return `https://gitlab.com/api/v4/projects/${encodeURIComponent(
          matches[1]
        )}/repository/commits`;
      } else if (link.toLowerCase().includes("github")) {
        const matches = link.match(githubRegEx);
        console.log(matches[1]);
        setCategory("github");
        return `https://api.github.com/repos/${encodeURI(matches[1])}/commits`;
      } else {
        setCategory("");
      }
    }
  }

  let baseGitHub = "";
  //For storing chat when fetched
  const [commits, setCommits] = useState();

  function parseISOString(s) {
    var b = s.split(/\D+/);
    return new Date(Date.UTC(b[0], --b[1], b[2], b[3], b[4], b[5], b[6]));
  }

  function getCategory(category) {
    switch (category) {
      case "gitlab":
        return (
          <div>
            <h5> Seneste commits til {props.project.name} GitLab Repo</h5>

            {commits.map(commit => {
              const date = parseISOString(commit.created_at);
              return (
                <div className={styles.commitContainer} key={commit.id}>
                  <p>{commit.message}</p>
                  <div className="d-flex justify-content-between">
                    <span>
                      {commit.author_name}, &nbsp;
                      {getDay(date.getDay()) + " "} {date.getDate()}.
                      {date.getMonth() + 1} {date.getHours()}:
                      {padToTwo(date.getMinutes())}
                    </span>
                    <p>
                      <a style={{ color: "#1abc9c" }} href={commit.web_url}>
                        Se på GitLab &nbsp;
                        <i className="fab fa-gitlab"></i>
                      </a>
                    </p>
                  </div>
                </div>
              );
            })}
          </div>
        );
      case "github":
        return (
          <div>
            <h5> Commits til {props.project.name} GitLab Repo</h5>

            {commits.map(commit => {
              const date = parseISOString(commit.commit.author.date);
              return (
                <div className={styles.commitContainer} key={commit.node_id}>
                  <p>{commit.commit.message}</p>
                  <div className="d-flex justify-content-between">
                    <span>
                      {commit.commit.author.name}, &nbsp;
                      {getDay(date.getDay()) + " "} {date.getDate()}.
                      {date.getMonth() + 1} {date.getHours()}:
                      {padToTwo(date.getMinutes())}
                    </span>
                    <p>
                      <a style={{ color: "#1abc9c" }} href={commit.html_url}>
                        Se på GitHub &nbsp;
                        <i className="fab fa-github"></i>
                      </a>
                    </p>
                  </div>
                </div>
              );
            })}
          </div>
        );

      default:
        return (
          <div>
            <p> Problemer med å laste inn commits </p>
          </div>
        );
    }
  }

  function getDay(dayOfTheWeek) {
    console.log(dayOfTheWeek);
    switch (dayOfTheWeek) {
      case 1:
        return "Man";
      case 2:
        return "Tir";
      case 3:
        return "Ons";
      case 4:
        return "Tor";
      case 5:
        return "Fre";
      case 6:
        return "Lør";
      case 0:
        return "Søn";

      default:
        break;
    }
  }

  function padToTwo(number) {
    if (number <= 9) {
      number = ("0" + number).slice(-2);
    }
    return number;
  }

  useEffect(() => {
    if (props.project.git !== null) {
      const url = getApiLink(props.project.git);
      async function fetchCommits() {
        try {
          const commitRespnse = await axios(url);
          console.log(commitRespnse);
          setCommits(commitRespnse.data);
        } catch (error) {
          console.error(error);
        }
      }
      fetchCommits();
    }
  }, [props.project]);

  if (props.project.git === null) {
    return <div></div>;
  }

  if (commits === undefined) {
    return <div> Laster inn linkområdet </div>;
  } else {
    return (
      <div
        className={cx(
          globalStyles.row,
          styles.projectItem,
          globalStyles["justify-content-right"]
        )}
      >
        {isAuthenticated && (
          <>
            <div className={cx(globalStyles["col-md"], styles.description)}>
              {getCategory(category)}
            </div>
          </>
        )}
      </div>
    );
  }
}

export default ProjectLink;
