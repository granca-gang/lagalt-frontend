import React, { useState, useEffect, useRef } from "react";
import { FaEdit } from "react-icons/fa";
import globalStyles from "../../Assets/global-styles/bootstrap.module.css";
import cx from "classnames";
import axios from "axios";
import styles from "./Project.module.css";
import LabelList from "../LabelList/LabelList";
import ListButton from "../ListButton/ListButton";
import { Link, Router } from "react-router-dom";
import { useAuth0 } from "../../react-auth0-spa";
import Axios from "axios";
import { getIdFromSub } from "../../Utils/utils";
import ProjectMember from "../ProjectMember/ProjectMember";
import { isEmptyObject } from "jquery";
import ProjectLink from "../ProjectLink/ProjectLink";

function Project(props, { match }) {
  const { isAuthenticated, user, getTokenSilently } = useAuth0();
  const [project, setProject] = useState({
    chat: {},
    category: {},
    labels: []
  });

  const [isLoading, setIsLoading] = useState(true);

  //For storing dBUser
  const [dbUser, setDbUser] = useState({});

  //For contolling if user has sen an application
  const [apply, setApply] = useState(false);
  const [status, setStatus] = useState("");

  const [owner, setOwner] = useState();

  //For storing an application
  const [application, setApplication] = useState({});

  //Fires if user clicks "Bli med" button
  const handleApplication = event => {
    setApply(true);
  };

  //Fires if user clicks "Send" for application message
  //Posts application to https://lagaltbackend.herokuapp.com/projects/${project.id}/users.
  const postApplication = async () => {
    console.log(application);
    try {
      const token = await getTokenSilently();

      console.log(application);
      const response = await Axios.post(
        `https://lagaltbackend.herokuapp.com/projects/${project.id}/users`,
        application,
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      );
      setStatus("APPLICANT");
    } catch (error) {
      console.error(error);
    }
  };

  //UseEffect to fetch data about owner of project
  useEffect(() => {
    async function fetchOwner() {
      if (project.ownerId !== undefined) {
        const ownerResponse = await Axios(
          `https://lagaltbackend.herokuapp.com/users/${project.ownerId}`
        );

        setOwner(ownerResponse.data);
        console.log("Owner", ownerResponse.data);
      }
    }

    fetchOwner();
  }, [project.ownerId]);

  //UseEffect to fetch project data
  useEffect(() => {
    async function fetchProject() {
      try {
        const projectResponse = await axios(
          `https://lagaltbackend.herokuapp.com/projects/${props.id}`
        );
        setProject(projectResponse.data);
      } catch (error) {
        console.error(error);
      }
    }
    fetchProject();
  }, [props.id]);

  //UseEffect to fetch user from DB based on Auth0 user.
  useEffect(() => {
    async function fetchDbUser() {
      try {
        if (!isEmptyObject(user)) {
          const userResponse = await axios(
            `https://lagaltbackend.herokuapp.com/users/${getIdFromSub(
              user.sub
            )}`
          );
          setDbUser(userResponse.data);
        }
      } catch (error) {
        console.error(error);
      }
    }
    fetchDbUser();
  }, [user]);

  //UseEffect to get status of user if user exists
  useEffect(() => {
    async function fetchStatus() {
      try {
        if (!isEmptyObject(user)) {
          const statusResponse = await axios(
            `https://lagaltbackend.herokuapp.com/projects/${
              props.id
            }/${getIdFromSub(user.sub)}`
          );
          if (statusResponse.data === "") {
            setStatus("NULL");
          } else {
            setStatus(statusResponse.data);
          }
        }
      } catch (error) {
        console.error(error);
      }
    }
    fetchStatus();
  }, [props.id, user]);

  //Posts to `https://lagaltbackend.herokuapp.com/projects/visited` to log when
  // a logged in user visits a project.
  useEffect(() => {
    async function postToVisited() {
      if (!isEmptyObject(dbUser) && project.id && isAuthenticated)
        try {
          const token = await getTokenSilently();
          console.log("Trying to set visited");
          const response = await Axios.put(
            `https://lagaltbackend.herokuapp.com/projects/visited`,
            {
              user: dbUser,
              project: project
            },

            {
              headers: {
                Authorization: `Bearer ${token}`
              }
            }
          );
        } catch (error) {
          console.error(error);
        }
    }
    postToVisited();
  }, [dbUser, getTokenSilently, isAuthenticated, project]);

  if (project === undefined) {
    return <div>Henter prosjekt, vennligst vent..</div>;
  } else {
    return (
      <div className={styles.container}>
        <div className={"container"}>
          <h1>
            {project.name} {console.log(status)}
            {status === "ADMIN" && (
              <Link
                className={styles.edit}
                to={`/prosjekt/${props.id}/endre`}
                key={project.id}
              >
                <FaEdit />
              </Link>
            )}
          </h1>
          <div
            className={cx(
              globalStyles.row,
              globalStyles["justify-content-center"],
              styles.projectItem
            )}
          >
            <div className={cx(globalStyles["col-md-3"], styles.description)}>
              <div>
                <img
                  className={styles.image}
                  src={project.picture}
                  alt={project.name}
                ></img>
                <h5>Informasjon</h5>
                {owner && (
                  <ul className="list-group">
                    <Link
                      to={`/profil/${owner.id}`}
                      style={{ textDecoration: "none", color: "white" }}
                    >
                      <li
                        className={cx(
                          globalStyles["list-group-item"],
                          styles.usersListItem
                        )}
                      >
                        Leder: {owner.name}
                      </li>
                    </Link>
                    <li
                      className={cx(
                        globalStyles["list-group-item"],
                        styles.usersListItem
                      )}
                    >
                      Status: {project.status}
                    </li>
                    {project.link && (
                      <li
                        className={cx(
                          globalStyles["list-group-item"],
                          styles.usersListItem
                        )}
                      >
                        <a
                          style={{ textDecoration: "none" }}
                          href={project.link}
                        >
                          {" "}
                          Hjemmeside{" "}
                        </a>
                      </li>
                    )}
                  </ul>
                )}
              </div>
            </div>
            <div className={cx(globalStyles["col-md-6"], styles.description)}>
              <label>
                <h5>Beskrivelse</h5>
                <p style={{ width: "100%", minHeight: "200px" }}>
                  {project.description}
                </p>
              </label>
              <h5>Ferdigheter:</h5>
              <LabelList labels={project.labels} />
            </div>
            <div className={cx(globalStyles["col-md"], styles.description)}>
              <div>
                <h5>Kategorier</h5>

                <ul className="list-group">
                  <ListButton
                    key={project.category.id}
                    value={project.category.description}
                  />
                </ul>
              </div>
            </div>
            {/*Displays application area if user is logged in, but noe a member of the project*/}
            {status === "NULL" && (
              <div
                className={cx(
                  globalStyles["col-md-6"],
                  globalStyles["text-center"],
                  styles.projectItem
                )}
              >
                {/*If user chooses to apply, the user is shown an area to write an application and send it*/}
                {!apply && (
                  <div>
                    <button
                      className={cx(
                        globalStyles.btn,
                        globalStyles["btn-lg"],
                        globalStyles["btn-block"],
                        styles.btn,
                        styles.customButton,
                        styles.joinButton
                      )}
                      onClick={() => handleApplication()}
                    >
                      Bli med!
                    </button>
                  </div>
                )}

                {apply && (
                  <div>
                    <label>
                      <h5>Skriv en kort søknad til leder av prosjektet! </h5>
                      <br />
                      <textarea
                        className={styles.newMessage}
                        type="text"
                        value={application.applicationText}
                        onChange={event =>
                          setApplication({
                            user: dbUser,
                            project: project,
                            applicationText: event.target.value,
                            role: "APPLICANT"
                          })
                        }
                      ></textarea>
                    </label>
                    <br />
                    <button
                      className={cx(
                        globalStyles.btn,
                        globalStyles["btn-lg"],
                        styles.btn,
                        styles.customButton,
                        styles.joinButton
                      )}
                      data-toggle="modal"
                      data-target="#accept"
                      disabled={isEmptyObject(application)}
                    >
                      Send!
                    </button>
                    <div
                      class="modal fade"
                      id="accept"
                      tabindex="-1"
                      role="dialog"
                      aria-labelledby="acceptLabel"
                      aria-hidden="true"
                    >
                      {/*When a user clicks "Send" a popup appears and asks the user to confirm sharing his information*/}
                      <div
                        class="modal-dialog"
                        role="document"
                        style={{ color: "black" }}
                      >
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="accpetLabel">
                              Godkjenning
                            </h5>
                            <button
                              type="button"
                              class="close"
                              data-dismiss="modal"
                              aria-label="Close"
                            >
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <p>
                              Ved å sende en søknad godtar du at leder av
                              prosjektet får tilgang til din fulle profil, samt
                              søknadstekst.
                            </p>
                          </div>
                          <div class="modal-footer">
                            <button
                              type="button"
                              class="btn btn-secondary"
                              data-dismiss="modal"
                            >
                              Avslå
                            </button>
                            <form>
                              <button
                                type="button"
                                class="btn btn-success"
                                data-dismiss="modal"
                                onClick={event => postApplication()}
                              >
                                Godkjenn
                              </button>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
              </div>
            )}

            {status === "APPLICANT" && (
              <h5>Søknad er sendt. Venter på godkjenning fra leder.</h5>
            )}
          </div>

          {/* For authenticated and members of project*/}
          {(status === "MEMBER" || status === "ADMIN") && isAuthenticated && (
            <>
              <ProjectMember project={project} dbUser={dbUser} />
              <ProjectLink project={project} dbUser={dbUser}></ProjectLink>
            </>
          )}
        </div>
      </div>
    );
  }
}

export default Project;
