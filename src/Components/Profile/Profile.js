import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import globalStyles from "../../Assets/global-styles/bootstrap.module.css";
import cx from "classnames";
import styles from "./Profile.module.css";
import axios from "axios";
import LabelList from "../LabelList/LabelList";
import ListButton from "../ListButton/ListButton";
import { getIdFromSub } from "../../Utils/utils";
import { useAuth0 } from "../../react-auth0-spa";
import { Link } from "react-router-dom";
import { FaEdit } from "react-icons/fa";
import { isEmptyObject } from "jquery";

function Profile(props) {
  const { isLoading, isAuthenticated, user, getTokenSilently } = useAuth0();

  const [dbUser, setDbUser] = useState({
    labels: []
  });

  const [userProjects, setUserProjects] = useState({});

  useEffect(() => {
    async function fetchDbUser() {
      try {
        if (!isEmptyObject(user)) {
          const userResponse = await axios(
            `https://lagaltbackend.herokuapp.com/users/${getIdFromSub(
              user.sub
            )}`
          );
          setDbUser(userResponse.data);
        }
      } catch (error) {
        console.error(error);
      }
    }
    fetchDbUser();
  }, [user]);

  useEffect(() => {
    async function fetchUserProjects() {
      try {
        const response = await axios(
          `https://lagaltbackend.herokuapp.com/users/${getIdFromSub(
            user.sub
          )}/projects`
        );
        setUserProjects(response.data);
      } catch (error) {
        console.error(error);
      }
    }
    fetchUserProjects();
  }, [user]);

  return (
    <div className={styles.profile}>
      <h1>
        Brukerprofil {"  "}
        <Link className={styles.edit} to={`/profil/endre`} key={dbUser.id}>
          <FaEdit />
        </Link>
      </h1>
      {!isLoading && user && (
        <div className={globalStyles.row}>
          <div className={cx(styles["user-profile"], globalStyles["col-md-8"])}>
            <h3>{user.name}</h3>
            {dbUser.hidden && <h5>(Skjult profil)</h5>}
            <div className={globalStyles.row}>
              <div className={globalStyles["col-md-8"]}>
                <p>E-mail: {user.email}</p>
                <div className={styles["user-info"]}>
                  <p>{dbUser.description}</p>
                </div>
              </div>
              <div className={globalStyles["col-sm-4"]}>
                <figure className={globalStyles.figure}>
                  <img
                    className={cx(globalStyles["figure-img"], styles.image)}
                    src={user.picture}
                    alt="User"
                  ></img>
                </figure>
              </div>
            </div>

            <div className={globalStyles.row}>
              <div
                className={cx(globalStyles["col-12"], styles["user-skills"])}
              >
                <LabelList labels={dbUser.labels} />
              </div>
            </div>
          </div>
          <div className={cx(globalStyles["col-sm"], styles["user-project"])}>
            <h4>Brukerprosjekter</h4>
            <ul className="list-group">
              {Object.keys(userProjects).map((key, i) => {
                return (
                  <ListButton
                    value={userProjects[key].name}
                    to={`/prosjekt/${userProjects[key].id}`}
                    key={userProjects[key].id}
                  />
                );
              })}
            </ul>
          </div>
        </div>
      )}
    </div>
  );
}
export default Profile;
