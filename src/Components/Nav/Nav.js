import React, { useState, useEffect } from "react";
import "../../App.css";
import { useAuth0 } from "../../react-auth0-spa";
import { Link, BrowserRouter } from "react-router-dom";
import styles from "./Nav.module.css";
import cx from "classnames";
import globalStyles from "../../Assets/global-styles/bootstrap.module.css";

function Nav(props) {
  const { isAuthenticated, loginWithPopup, logout, user } = useAuth0();

  const [search, setSearch] = useState("");

  useEffect(() => {
    props.onChange(search);
  }, [props, search]);

  return (
    <div>
      <nav
        className={cx("navbar", "navbar-expand-lg", styles["navbar-custom"])}
      >
        <p className="navbar-brand" style={{ marginTop: "13px" }}>
          <Link
            className={styles.Link}
            style={{ textDecoration: "none" }}
            to="/"
          >
            LagAlt
          </Link>
        </p>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNavDropdown"
          aria-controls="navbarNavDropdown"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNavDropdown">
          <ul className="navbar-nav aling-items-center mr-auto">
            <li className="nav-item active">
              <Link className={styles.Link} to="/">
                Hjem
              </Link>
            </li>
            <li className="nav-item">
              <Link className={styles.Link} to="/opprett/prosjekt">
                Nytt Prosjekt
              </Link>
            </li>
          </ul>
          <input
            className="form-control"
            type="text"
            placeholder="Søk"
            value={search}
            onChange={event => setSearch(event.target.value)}
            style={{ width: "60%", marginRight: "50px", marginLeft: "50px" }}
          ></input>
          <ul className="navbar-nav aling-items-right">
            {!isAuthenticated && (
              <button
                style={{
                  width: "100px",
                  height: "40px",
                  margin: "8px",
                  marginLeft: "100px"
                }}
                onClick={() => loginWithPopup({})}
              >
                Logg inn
              </button>
            )}

            {isAuthenticated && user && (
              <li className="nav-item dropdown" style={{ marginLeft: "46px" }}>
                <p
                  className="nav-link dropdown-toggle"
                  id="navbarDropdownMenuLink"
                  role="button"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  {user.given_name} &nbsp;
                  <img
                    src={user.picture}
                    alt={user.name}
                    width="40"
                    height="40"
                    className="rounded-circle"
                  />
                </p>
                <div
                  className="dropdown-menu dropdown-menu-right"
                  aria-labelledby="navbarDropdownMenuLink"
                >
                  <Link
                    className={cx(
                      styles.dropdownLink,
                      globalStyles["dropdown-item"]
                    )}
                    to="/opprett/prosjekt"
                  >
                    Nytt Prosjekt
                  </Link>

                  <Link
                    className={cx(
                      styles.dropdownLink,
                      globalStyles["dropdown-item"]
                    )}
                    to="/profil"
                  >
                    Profil
                  </Link>

                  <p
                    className="dropdown-item"
                    onClick={() =>
                      logout({
                        returnTo: window.location.origin
                      })
                    }
                  >
                    Logg ut
                  </p>
                </div>
              </li>
            )}
          </ul>
        </div>
      </nav>
    </div>
  );
}

export default Nav;
