import React, { Component } from "react";
import Axios from "axios";

class CreateProject extends Component {
  constructor(props) {
    super(props);
    this.state = { name: "", ownerId: "", status: "", description: "" };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    console.log(this.state);
    Axios.post("https://lagaltbackend.herokuapp.com/projects", this.state).then(
      alert(JSON.stringify(this.state) + "was submitted")
    );
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  render() {
    return (
      <div
        style={{
          width: "300px",
          textAlign: "right",
          marginLeft: "auto",
          marginRight: "auto"
        }}
      >
        <form onSubmit={this.handleSubmit} style={{ color: "white" }}>
          <label>
            Navn:
            <input
              name="name"
              type="text"
              value={this.state.name}
              onChange={this.handleInputChange}
            />
          </label>
          <br></br>
          <label>
            EierID:
            <input
              name="ownerId"
              type="text"
              value={this.state.ownerID}
              onChange={this.handleInputChange}
            />
          </label>
          <br></br>
          <label>
            Status:
            <input
              name="status"
              value={this.state.status}
              onChange={this.handleInputChange}
            />
          </label>
          <br></br>
          <label>
            Beskrivelse:
            <textarea
              name="description"
              type="text"
              value={this.state.description}
              onChange={this.handleInputChange}
            />
          </label>
          <br></br>
          <input type="submit" value="Send" />
        </form>
      </div>
    );
  }
}

export default CreateProject;
