import React, { Component } from "react";
import Axios from "axios";

class CreateUser extends Component {
  constructor(props) {
    super(props);
    this.state = { name: "", email: "", username: "", role: "" };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    console.log(this.state);
    Axios.post("https://lagaltbackend.herokuapp.com/users", this.state).then(
      alert(JSON.stringify(this.state) + "was submitted")
    );
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  render() {
    return (
      <div
        style={{
          width: "300px",
          textAlign: "right",
          marginLeft: "auto",
          marginRight: "auto"
        }}
      >
        <form onSubmit={this.handleSubmit} style={{ color: "white" }}>
          <label>
            Navn:
            <input
              name="name"
              type="text"
              value={this.state.name}
              onChange={this.handleInputChange}
            />
          </label>
          <br></br>
          <label>
            Email:
            <input
              name="email"
              type="text"
              value={this.state.email}
              onChange={this.handleInputChange}
            />
          </label>
          <br></br>
          <label>
            Brukernavn:
            <input
              name="username"
              value={this.state.username}
              onChange={this.handleInputChange}
            />
          </label>
          <br></br>
          <label>
            Rolle:
            <input
              name="role"
              type="text"
              value={this.state.role}
              onChange={this.handleInputChange}
            />
          </label>
          <br></br>
          <input type="submit" value="Send" />
        </form>
      </div>
    );
  }
}

export default CreateUser;
