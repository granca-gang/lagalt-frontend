import React, { useState, useEffect, useRef } from "react";
import globalStyles from "../../Assets/global-styles/bootstrap.module.css";
import cx from "classnames";
import axios from "axios";
import styles from "./ProjectMember.module.css";
import ListButton from "../ListButton/ListButton";
import { Link, Router } from "react-router-dom";
import { useAuth0 } from "../../react-auth0-spa";
import Axios from "axios";
import { getIdFromSub } from "../../Utils/utils";
import { isEmptyObject } from "jquery";

const regex = /T(.{5})/;

function ProjectMember(props, { match }) {
  const { isAuthenticated, getTokenSilently } = useAuth0();

  //For storing chat when fetched
  const [chat, setChat] = useState({ content: [] });

  //For contolling if user has sen an application
  const [members, setMembers] = useState();

  //Stores and sets message to be posted to API on behalf of authenticated user.
  const [message, setMessage] = useState({ message: "" });

  //For controlling when to scroll to bottom
  const [shouldScroll, setShouldScroll] = useState(true);

  //Enables auto scroll on chat update
  const messagesEndRef = useRef(null);
  const scrollToBottom = () => {
    console.log(shouldScroll);
    if (shouldScroll && messagesEndRef.current !== null) {
      messagesEndRef.current.scrollTop = messagesEndRef.current.scrollHeight;
    }
    /* messagesEndRef.current.scrollIntoView({
      top: 200,
      behavior: "smooth"
    });
    */
  };

  const [seconds, setSeconds] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setSeconds(seconds => seconds + 1);
    }, 1000);
    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    async function fetchMembers() {
      if (props.project !== undefined) {
        const memberResponse = await axios(
          `https://lagaltbackend.herokuapp.com/projects/${props.project.id}/members`
        );
        console.log(memberResponse);
        setMembers(memberResponse.data);
      }
    }
    fetchMembers();
  }, [props.project]);

  useEffect(() => {
    async function fetchChat() {
      if (props.project.chat.id !== undefined) {
        const result = await axios(
          `https://lagaltbackend.herokuapp.com/chats/${props.project.chat.id}/messages`
        );
        let chatReversed = result.data;
        chatReversed.content = chatReversed.content.reverse();
        if (chatReversed.totalElements !== chat.totalElements) {
          setChat(chatReversed);
          console.log("New chat was set");
          scrollToBottom();
        }
      }
    }

    fetchChat();
  }, [seconds]);

  function getImageLink(id) {
    const member = members.find(member => member.id === id);
    if (member !== undefined && member.picture !== null) {
      return member.picture;
    } else {
      return "https://miquon.org/wp-content/uploads/2016/02/GenericUser.png";
    }
  }

  function getNameFromMember(id) {
    const member = members.find(member => member.id === id);

    if (member !== undefined && member.name !== null) {
      return member.name.split(" ")[0];
    } else {
      return "Anonymous";
    }
  }

  function checkScrollPosiotion() {
    setShouldScroll(false);
    //setTimeout(setShouldScroll(true), 3000);
  }

  const postMessage = async () => {
    try {
      const token = await getTokenSilently();

      const response = await Axios.post(
        `https://lagaltbackend.herokuapp.com/chats/${props.project.chat.id}/messages`,
        message,
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      );
      setShouldScroll(true);
      setMessage({ message: "" });
    } catch (error) {
      console.error(error);
    }
  };

  if (chat === undefined) {
    return <div> Laster inn medlemsområdet </div>;
  } else {
    return (
      <div
        className={cx(
          globalStyles.row,
          styles.projectItem,
          globalStyles["justify-content-right"]
        )}
      >
        {isAuthenticated && members && (
          <>
            <div className={cx(globalStyles["col-md-3"], styles.description)}>
              <h5>Medlemmer</h5>
              <ul className="list-group" style={{ textDecoration: "none" }}>
                {members.map(member => {
                  return (
                    <ListButton
                      key={member.id}
                      value={member.name}
                      to={`../profil/${member.id}`}
                      //key={member.id}
                    />
                  );
                })}
              </ul>
            </div>
            <div className={cx(globalStyles["col"], styles.description)}>
              <h5> {props.project.chat.title} </h5>
              <div
                className={styles.chatBox}
                ref={messagesEndRef}
                onScroll={() => checkScrollPosiotion()}
                onMouseLeave={() => {
                  setShouldScroll(true);
                }}
              >
                {chat &&
                  chat.content.map(chatMessage => {
                    if (chatMessage.userId === props.dbUser.id) {
                      return (
                        <div
                          className={cx(styles.chatContainer, styles.darker)}
                          key={chatMessage.id}
                        >
                          <img
                            src={getImageLink(chatMessage.userId)}
                            alt={getNameFromMember(chatMessage.userId)}
                          />
                          <h6>{getNameFromMember(chatMessage.userId)}</h6>

                          <p>{chatMessage.message}</p>
                          <span className={styles.timeRight}>
                            {chatMessage.createdAt.match(regex)[1]}
                          </span>
                        </div>
                      );
                    } else {
                      return (
                        <div
                          className={styles.chatContainer}
                          key={chatMessage.id}
                        >
                          <img
                            src={getImageLink(chatMessage.userId)}
                            alt={getNameFromMember(chatMessage.userId)}
                            className={styles.right}
                          />

                          <h6>{getNameFromMember(chatMessage.userId)}</h6>

                          <p>{chatMessage.message}</p>

                          <span className={styles.timeLeft}>
                            {chatMessage.createdAt.match(regex)[1]}
                          </span>
                        </div>
                      );
                    }
                  })}
                <div />
              </div>

              <div className="row">
                <div className="col-md-10">
                  <textarea
                    className={styles.newMessage}
                    type="text"
                    value={message.message}
                    onChange={event =>
                      setMessage({
                        userId: props.dbUser.id,
                        message: event.target.value
                      })
                    }
                  ></textarea>
                </div>
                <div className="col-md-2">
                  <button
                    className={cx(
                      globalStyles.btn,
                      globalStyles["btn-lg"],
                      styles.btn,
                      styles.customButton
                    )}
                    onClick={() => postMessage()}
                    disabled={message.message === ""}
                  >
                    Send!
                  </button>
                </div>
              </div>
            </div>
          </>
        )}
      </div>
    );
  }
}

export default ProjectMember;
