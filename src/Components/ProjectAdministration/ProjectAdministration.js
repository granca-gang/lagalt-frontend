import React, { useEffect, useState, Component } from "react";
import cx from "classnames";
import globalStyles from "../../Assets/global-styles/bootstrap.module.css";
import styles from "./ProjectAdministration.module.css";
import CustomButton from "../CustomButton/CustomButton";
import LabelList from "../LabelList/LabelList";
import axios from "axios";
import { useAuth0 } from "../../react-auth0-spa";

/* eslint-disable no-unused-vars */

function ProjectAdministration(props) {
  const { isAuthenticated, user, getTokenSilently } = useAuth0();

  const [categories, setCategories] = useState({});

  const [category, setCategory] = useState({});

  const [labelList, setLabelList] = useState({});

  const [label, setLabel] = useState({});

  const [update, setUpdate] = useState({
    category: {},
    status: {},
    description: {},
    labels: []
  });

  useEffect(() => {
    setUpdate({
      id: props.project.id,
      name: props.project.name,
      ownerId: props.project.ownerId,
      category: props.project.category,
      status: props.project.status,
      description: props.project.description,
      picture: props.project.picture,
      link: props.project.link,
      git: props.project.git,
      labels: props.project.labels
    });
  }, [props.project]);

  //Submits a PUT request with updated project
  const handleSubmit = async e => {
    e.preventDefault();
    try {
      const token = await getTokenSilently();
      const response = await axios
        .put(`https://lagaltbackend.herokuapp.com/projects`, update, {
          headers: {
            Authorization: `Bearer ${token}`
          }
        })
        .then(alert("Prosjektet er oppdatert!"));
      console.log(response);
    } catch (error) {
      console.error(error);
    }
  };

  const handleInputChange = event => {
    setUpdate({
      ...update,
      [event.currentTarget.name]: event.currentTarget.value
    });
  };

  useEffect(() => {});

  const handleCategoryChange = description => {
    let category;
    var x;
    for (x in categories) {
      if (categories[x].description === description) {
        category = {
          id: categories[x].id,
          description: categories[x].description
        };
      }
    }
    setUpdate({
      ...update,
      category: category
    });
  };

  //Add label to project
  const addLabel = async () => {
    var x;
    var newLabel;
    for (x in labelList) {
      if (labelList[x].label === label.label) {
        update.labels.push({ id: labelList[x].id, label: labelList[x].label });
        newLabel = { id: labelList[x].id, label: labelList[x].label };
      }
    }
    setUpdate({
      ...update,
      labels: update.labels
    });
    try {
      const token = await getTokenSilently();
      console.log(token);
      const response = await axios.post(
        `https://lagaltbackend.herokuapp.com/projects/${props.project.id}/label`,
        newLabel,
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      );
    } catch (error) {
      console.log(error);
    }
  };

  //Fetch all categories in database to populate datalist for input selection
  useEffect(() => {
    async function fetchData() {
      const result = await axios(
        "https://lagaltbackend.herokuapp.com/categories"
      );
      console.log(result.data);
      setCategories(result.data);
    }
    fetchData();
  }, []);

  //Fetch all labels in database to populate datalist for input selection
  useEffect(() => {
    async function fetchData() {
      const result = await axios("https://lagaltbackend.herokuapp.com/labels");
      setLabelList(result.data);
    }
    fetchData();
  }, []);

  var inputs = document.querySelectorAll("input[list]");
  for (var i = 0; i < inputs.length; i++) {
    // When the value of the input changes...
    inputs[i].addEventListener("change", function() {
      var optionFound = false,
        datalist = this.list;
      // Determine whether an option exists with the current value of the input.
      for (var j = 0; j < datalist.options.length; j++) {
        if (this.value === datalist.options[j].value) {
          optionFound = true;
          break;
        }
      }
      // use the setCustomValidity function of the Validation API
      // to provide an user feedback if the value does not exist in the datalist
      if (optionFound) {
        this.setCustomValidity("");
      } else {
        this.setCustomValidity("Please select a valid value.");
      }
    });
  }

  return (
    <div className={cx("container", styles.container)}>
      <div
        className={cx(
          globalStyles.row,
          globalStyles["justify-content-center"],
          styles.projectItem
        )}
      >
        <div className={cx("col-md-8")}>
          <form
            className={cx(globalStyles["form-group"], styles.description)}
            onSubmit={handleSubmit}
          >
            <div className="row">
              <div className="col-md-5">
                <label className={styles.label}>
                  Navn:
                  <input
                    name="name"
                    className={cx(
                      globalStyles["form-control"],
                      styles["customInput"]
                    )}
                    type="text"
                    value={update.name}
                    required
                    onChange={handleInputChange}
                  />
                </label>
                <br />
                <label className={styles.label}>
                  Kategori:
                  <select
                    className={styles.customInput}
                    onChange={event => {
                      handleCategoryChange(event.target.value);
                    }}
                    id="categories"
                  >
                    <option defaultValue>{update.category.description}</option>
                    {Object.keys(categories).map((key, i) => {
                      return (
                        categories[key].id !== update.category.id && (
                          <option key={categories[key].id}>
                            {categories[key].description}
                          </option>
                        )
                      );
                    })}
                  </select>
                </label>
                <br></br>
                <label className={styles.label}>
                  Status:
                  <input
                    name="status"
                    className={cx(
                      globalStyles["form-control"],
                      styles["customInput"]
                    )}
                    value={update.status}
                    onChange={handleInputChange}
                  />
                </label>
              </div>
              <div className="col-md">
                <img
                  className={styles.img}
                  src={update.picture}
                  alt={update.name}
                ></img>
              </div>
            </div>
            <label className={styles.label}>
              Bilde:
              <input
                name="picture"
                required
                className={cx(
                  globalStyles["form-control"],
                  styles["customInput"]
                )}
                type="text"
                value={update.picture}
                onChange={handleInputChange}
              />
            </label>

            <label className={styles.label}>
              Link:
              <input
                name="link"
                className={cx(
                  globalStyles["form-control"],
                  styles["customInput"]
                )}
                type="text"
                value={update.link}
                onChange={handleInputChange}
              />
            </label>

            <label className={styles.label}>
              Git Repo:
              <input
                name="git"
                className={cx(
                  globalStyles["form-control"],
                  styles["customInput"]
                )}
                type="text"
                value={update.git}
                onChange={handleInputChange}
              />
            </label>

            <label>
              Beskrivelse:
              <textarea
                name="description"
                rows="5"
                cols="50"
                className={cx(
                  globalStyles["form-control"],
                  styles["form-textarea"],
                  styles["customInput"]
                )}
                value={update.description}
                onChange={handleInputChange}
                type="text"
                required
              />
            </label>
            <div
              style={{ position: "absolute", right: "20px", bottom: "20px" }}
            >
              <CustomButton type="submit" value="Send"></CustomButton>
            </div>
          </form>
        </div>
        <div className={cx("col-md-3")}>
          <div className={styles.description}>
            <label>
              <h4>Ferdigheter</h4>
              <input
                className={cx(
                  globalStyles["form-control"],
                  styles["customInput"]
                )}
                type="text"
                name="label"
                list="labels"
                id="project_labels"
                autoComplete="off"
                required
                value={label.label}
                onChange={event =>
                  setLabel({ id: event.target.key, label: event.target.value })
                }
              />
              <datalist className={styles.customInput} id="labels">
                {Object.keys(labelList).map((key, i) => {
                  return (
                    <option key={labelList[key].id}>
                      {labelList[key].label}
                    </option>
                  );
                })}
              </datalist>
              <CustomButton
                onClick={addLabel}
                type="button"
                value="Legg til"
              ></CustomButton>
              <LabelList labels={update.labels} />
            </label>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ProjectAdministration;
