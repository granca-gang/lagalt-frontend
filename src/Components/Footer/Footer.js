import React from "react";
import "../../App.css";
import cx from "classnames";
import globalStyles from "../../../node_modules/bootstrap/dist/css/bootstrap.css";
import { Link } from "react-router-dom";
import styles from "./Footer.module.css";

function Footer() {
  return (
    <div className={styles.footer}>
      <div className="row">
        <div className="col-sm">
          <p>LagAlt.no</p>
          <p></p>
          <p>Har du funnet en feil? Ta kontakt.</p>
          <a href="mailto:example@example.com">lagalt-devs@lagalt.no</a>
        </div>
        <div className="col-sm">
          <p>Mer om lagalt.no</p>
          <Link to={`/profil/endre`}>
            <p>Administrer din profil</p>
          </Link>
        </div>
        <div className="col-sm"></div>
      </div>
    </div>
  );
}

export default Footer;
