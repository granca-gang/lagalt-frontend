import React, { useState } from "react";
import "./App.css";
import Nav from "./Components/Nav/Nav";
import MainView from "./Views/MainView/MainView";
import ProjectView from "./Views/ProjectView/ProjectView";
import ProfileView from "./Views/ProfileView/ProfileView";
import { useAuth0 } from "./react-auth0-spa";
import history from "./Utils/history";
import PrivateRoute from "./Components/PrivateRoute/PrivateRoute";
import Footer from "./Components/Footer/Footer";
import ExternalApi from "./Views/ExternalApi";
import HttpsRedirect from "react-https-redirect";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams
} from "react-router-dom";
import CreateUser from "./Components/DataInput/CreateUser";
import CreateProject from "./Components/DataInput/CreateProject";
import CreateProjectView from "./Views/CreateProjectView/CreateProjectView";
import ProjectAdministrationView from "./Views/ProjectAdministrationView/ProjectAdministrationView";
import EditProfileView from "./Views/EditProfileView/EditProfileView";
import OtherProfileView from "./Views/OtherProfileView/OtherProfileView";

function App() {
  const { isLoading, user } = useAuth0();
  const [search, setSearch] = useState();
  return (
    <HttpsRedirect>
      <Router history={history}>
        <div className="App">
          <Nav onChange={e => setSearch(e)}></Nav>

          <Switch>
            <Route
              exact
              path="/"
              render={props => <MainView {...props} search={search} />}
            />
            <PrivateRoute
              path="/prosjekt/:id/endre"
              component={ProjectAdministrationView}
            />
            <Route path="/prosjekt/:id" component={ProjectView} />
            {/*<Route path="/profile/:id" component={ProfileView} />*/
            /*Vet ikke om dette er måten vi burde gjøre det på,
             * men det er en tech-demo på en måte :)
             */}
            <PrivateRoute path="/profil/endre" component={EditProfileView} />
            <Route path="/profil/:id" component={OtherProfileView} />
            <PrivateRoute path="/profil" component={ProfileView} />

            {/* NEW - add a route to the ExternalApi component */}
            <PrivateRoute path="/external-api" component={ExternalApi} />

            <Route path="/data/user" component={CreateUser} />
            <Route path="/data/project" component={CreateProject} />
            <PrivateRoute
              path="/opprett/prosjekt"
              component={CreateProjectView}
            />
          </Switch>
          <Footer></Footer>
        </div>
      </Router>
    </HttpsRedirect>
  );
}

export default App;
